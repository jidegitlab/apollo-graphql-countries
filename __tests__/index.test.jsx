import { render, screen, fireEvent } from '@testing-library/react'
import preloadAll from 'jest-next-dynamic'
import Home from '../pages/index'
import '@testing-library/jest-dom'

// this will make sure next/dynamic works properly with our test.
beforeAll(async () => {
  await preloadAll()
})

let countries = [
  {
    code: "Asp",
    name: "Asparigus",
    emoji: "😱️"
  },
  {
    code: "Ora",
    name: "Oramiyan",
    emoji: "⛈️"
  },
  {
    code: "Kek",
    name: "Kekuru",
    emoji: "🧟‍♂️️"
  }
]

describe('Home', () => {
  it('renders a heading', () => {
    render(<Home countries={countries}/>)

    const heading = screen.getByRole('heading', {
      name: /welcome to next\.js!/i,
    })

    expect(heading).toBeInTheDocument()
  })
})