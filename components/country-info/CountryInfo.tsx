import React from 'react'
import Layout from '../layout'
import { keygen } from '../../utils/keygen'

type LanguageType = {
  name: string
}

type StateType = {
  name: string
}

type ContinentType = {
  name: string
  countries: CountryType[]
}

type CountryType = {
  info : {
    name: string
    capital: string
    native: string
    states: StateType[]
    phone: string
    languages: LanguageType[]
    currency: string
    continent: ContinentType
  }
  name: string
}

type DetailsType = {
  name: string
}

function Details({data}:{data: DetailsType[]}) {
  data.length === 0 &&  <p className="item">Not Available!</p>
  return (
    <>
      {data.map(cur => 
        <p key={keygen()} className="item">
          {cur.name}{" "}
        </p>
      )}
    </>
  )
}


export default function CountryInfo({ info }: CountryType) {
  
  return (
    <Layout>
      <header><h1>{info.name}</h1></header>
      <p>Capital: {info.capital}</p>
      <p>Native Name: {info.native}</p>
      <p>Phone Code: {info.phone}</p>
      <div id="main">
        <section>
          <div className="block">
            <h3>Languages</h3>
            <Details data={info.languages}/>
          </div>
          <div className="block">
            <h3>Currency</h3>
            <p className="item">{info.currency ? info.currency : "Not Available!"}</p>
          </div>
          <div className="block">
            <h3>States</h3>
            <Details data={info.states}/>
          </div>
          <div className="block">
            <h3>Continent</h3>
            <p className="item">{info.continent.name}</p>
          </div>
          <div className="block">
            <h3>Neigbouring Countries</h3>
            <Details data={info.continent.countries}/>
          </div>
        </section>
      </div>
    </Layout>
  );
}

