export type MockedT<T> = T &
  {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [K in keyof T]: T[K] extends (...args: any[]) => any
      ? jest.Mock<ReturnType<T[K]>, Parameters<T[K]>> & T[K]
      : T[K]
  }

export const excludeValues = ['then', Symbol.toStringTag, Symbol.iterator]

// eslint-disable-next-line @typescript-eslint/ban-types
export const mockOf = <T extends object>(
  target: Partial<T> = {},
  excludes = excludeValues,
  // eslint-disable-next-line @typescript-eslint/ban-types
  constructor: { prototype: object } | undefined = undefined
): MockedT<T> =>
  new Proxy(target, {
    get: (target, property, ...rest) => {
      const cachedValue = Reflect.get(target, property, ...rest)
      if (
        cachedValue !== undefined ||
        excludes.find((excluded) => excluded === property)
      ) {
        return cachedValue
      }
      const newMock = jest.fn()
      Reflect.set(target, property, newMock)
      return newMock
    },
    set: (target, property, value, receiver) =>
      Reflect.set(target, property, value, receiver),
    getPrototypeOf: constructor ? () => constructor.prototype : undefined,
  }) as MockedT<T>
