export function mockOfFunction<T extends (...args: unknown[]) => unknown>(
  f: T
): jest.Mock<ReturnType<T>> {
  return f as unknown as jest.Mock<ReturnType<T>>
}
